class AddScriptIdToResourceScript < ActiveRecord::Migration
  def change
    add_column :resource_scripts, :script_id, :integer
  end
end
