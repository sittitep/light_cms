class AddStyleSheetIdToResourceStylesheet < ActiveRecord::Migration
  def change
    add_column :resource_stylesheets, :stylesheet_id, :integer
  end
end
