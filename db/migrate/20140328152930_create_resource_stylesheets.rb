class CreateResourceStylesheets < ActiveRecord::Migration
  def change
    create_table :resource_stylesheets do |t|
      t.integer :styleable_id
      t.text :styleable_type

      t.timestamps
    end
  end
end
