class CreateScripts < ActiveRecord::Migration
  def change
    create_table :scripts do |t|
      t.string :name
      t.string :cdn_link
      t.text :content, :limit => 4294967295
      t.integer :user_id
      t.string :scriptable_type
      t.integer :scriptable_id

      t.timestamps
    end
  end
end
