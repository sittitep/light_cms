class AddUserIdToStylesheet < ActiveRecord::Migration
  def change
    add_column :stylesheets, :user_id, :integer
  end
end
