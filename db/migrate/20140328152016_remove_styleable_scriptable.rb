class RemoveStyleableScriptable < ActiveRecord::Migration
  def change
    remove_column :stylesheets, :styleable_id
    remove_column :stylesheets, :styleable_type
    remove_column :scripts, :scriptable_id
    remove_column :scripts, :scriptable_type
  end
end
