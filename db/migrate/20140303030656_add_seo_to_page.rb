class AddSeoToPage < ActiveRecord::Migration
  def change
    add_column :pages, :seo_title, :string
    add_column :pages, :seo_description, :text
    add_column :pages, :seo_keywords, :text
    add_column :pages, :seo_noindex, :boolean
    add_column :pages, :seo_nofollow, :boolean
    add_column :pages, :seo_canonical, :string
    add_column :pages, :seo_prev, :string
    add_column :pages, :seo_next, :string
  end
end
