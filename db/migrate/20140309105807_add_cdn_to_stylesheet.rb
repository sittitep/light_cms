class AddCdnToStylesheet < ActiveRecord::Migration
  def change
    add_column :stylesheets, :cdn_link, :string
  end
end
