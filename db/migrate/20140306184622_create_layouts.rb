class CreateLayouts < ActiveRecord::Migration
  def change
    create_table :layouts do |t|
      t.string :name
      t.text :content, :limit => 4294967295
      t.integer :user_id

      t.timestamps
    end
  end
end
