class CreateStylesheets < ActiveRecord::Migration
  def change
    create_table :stylesheets do |t|
      t.string :name
      t.text :content, :limit => 4294967295
      t.integer :styleable_id
      t.string :styleable_type

      t.timestamps
    end
  end
end
