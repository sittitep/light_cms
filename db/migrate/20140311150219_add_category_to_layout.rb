class AddCategoryToLayout < ActiveRecord::Migration
  def change
    add_column :layouts, :category, :string
  end
end
