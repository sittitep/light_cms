class CreateResourceScripts < ActiveRecord::Migration
  def change
    create_table :resource_scripts do |t|
      t.integer :scriptable_id
      t.text :scriptable_type

      t.timestamps
    end
  end
end
