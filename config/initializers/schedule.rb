require 'rufus-scheduler'

scheduler = Rufus::Scheduler.new

scheduler.every '3h' do
  unless Rails.env.development?
    # do something every 3 hours
    Rails.logger.info "Restart Nginx #{Time.now}"
    system("service nginx restart")
  end
end

scheduler.every '1m' do
  unless Rails.env.production?
    # do something every 3 hours
    Rails.logger.info "Test by whois #{Time.now}"
    system("whois google.com")
  end
end