class Admin::UsersController < Admin::BaseController
  before_filter :flash_success, only: [:update]

  def edit
    @user = User.find params[:id]
  end

  def update
    @user = User.find params[:id]
    user_updater = UserUpdater.new(@user, params_user)
    @user = user_updater.user
    if @user.save
      redirect_to edit_admin_user_path(@user)
    else
      flash_errors(@user)
      render 'edit'
    end
  end

  private

    def params_user
      params.require(:user).permit!
    end
end
