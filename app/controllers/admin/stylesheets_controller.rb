class Admin::StylesheetsController < Admin::BaseController 
  before_filter :get_stylesheet, only: [:edit,:update,:destroy]
  before_filter :set_active_sidebar
  before_filter :flash_success, only: [:create,:update,:destroy]

  before_filter :only => [:update, :destroy] do |controller|
    controller.instance_eval do
      authorize(@stylesheet)
    end
  end
  
  def set_active_sidebar
    @active_sidebar = :css  
  end

  def index
    @stylesheets = current_user.stylesheets
  end

  def new
    @stylesheet = current_user.stylesheets.new
    parent_breadcrumb
    add_breadcrumb "New", :new_admin_stylesheet_path
  end

  def create
    @stylesheet = current_user.stylesheets.new stylesheet_params
    if @stylesheet.save
      redirect_to edit_admin_stylesheet_path(@stylesheet)
    else
      flash_errors(@stylesheet)
      render "new"
    end
  end

  def edit
    parent_breadcrumb
    add_breadcrumb "#{@stylesheet.name}", edit_admin_stylesheet_path(@stylesheet)
  end

  def update
    if @stylesheet.update_attributes stylesheet_params
      redirect_to edit_admin_stylesheet_path(@stylesheet)
    else
      flash_errors(@stylesheet)
      render "new"
    end
  end

  def destroy
    @stylesheet.delete
    redirect_to admin_stylesheets_path
  end

  private

    def stylesheet_params
      params.require(:stylesheet).permit!
    end

    def get_stylesheet
      @stylesheet = Stylesheet.find params[:id]
    end

    def parent_breadcrumb
      add_breadcrumb "Stylesheets", admin_stylesheets_path
    end
end
