class Admin::BaseController < ApplicationController
  layout "admin"
  include Admin::BaseHelper
  before_filter :authenticate_user!
  skip_filter :check_domain!
  add_breadcrumb "Admin", "#"

  MENU = [
    {name: :dashboard, path: {:controller => 'dashboard', :action =>'index'} },
    {name: :page, path: {:controller => 'pages', :action =>'index'}},
    {name: :layout, path: {:controller => 'layouts', :action =>'index'}},
    {name: :css, path: {:controller => 'stylesheets', :action =>'index'}},
    {name: :javascript, path: {:controller => 'scripts', :action =>'index'}}
  ]

  def flash_success
    flash["alert alert-success"] = "You successfully #{action_name} a #{controller_name.singularize}"
  end

  private
    def authorize(resource)
      redirect_to admin_dashboard_index_path unless current_user.id == resource.user_id
    end

    def flash_errors(resource)
      flash.delete("alert alert-success")
      @errors = resource.errors.full_messages
    end

    def update_asset(type,resource,new_asset_ids)
      new_asset_ids == nil ? new_asset_ids = [] : new_asset_ids = new_asset_ids.map{|x| x.to_i}
      current_assets_ids = resource.send("resource_"+type.pluralize).map{|x| x.send(type+"_id")}
      to_create_assets_ids = new_asset_ids - current_assets_ids      
      to_create_assets_ids.each do |id|
        resource.send("resource_"+type.pluralize).create((type+"_id").to_sym => id)
      end

      current_assets_ids.each do |id|
        unless new_asset_ids.include?(id)
          asset_type = ""
          type == "stylesheet" ? asset_type = "styleable_type" : asset_type = "scriptable_type"
          asset = ("resource_"+type).classify.constantize.where((type+"_id").to_sym => id, asset_type.to_sym => resource.class.name).first
          asset.delete
        end
      end
    end
end
