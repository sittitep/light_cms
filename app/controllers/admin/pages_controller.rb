class Admin::PagesController < Admin::BaseController
  add_breadcrumb "Page", :admin_pages_path

  before_filter :set_active_sidebar
  before_filter :get_page, only: [:edit,:update,:destroy]
  before_filter :flash_success, only: [:create,:update,:destroy]

  before_filter :only => [:update, :destroy] do |controller|
    controller.instance_eval do
      authorize(@page)
    end
  end

  # STATUS = ["Draft","Published","Hold"]
  STATUS = ["Published"]

  def set_active_sidebar
    @active_sidebar = :page  
  end

  def index
    @pages = current_user.pages.alive
  end

  def new
    @page = Page.new
    add_breadcrumb "New", :new_admin_page_path
  end

  def create
    @page = current_user.pages.new page_params
    if current_user.homepage.blank?
      @page.is_homepage = true
    end
    if @page.save
      redirect_to edit_admin_page_path(@page)
    else
      flash_errors(@page)
      render 'new'
    end
  end

  def edit
    add_breadcrumb @page.title, :edit_admin_page_path
  end

  def update

    update_asset("stylesheet", @page, page_params["stylesheet_ids"]) if page_params["stylesheet_update"]
    update_asset("script", @page, page_params["script_ids"]) if page_params["script_update"]

    if page_params[:is_homepage]
      current_homepage = current_user.homepage
      current_homepage.is_homepage = false
      current_homepage.save
    end
    
    if @page.update_attributes page_params
      redirect_to edit_admin_page_path(@page)
    else
      flash_errors(@page)
      render 'new'
    end
  end

  def destroy
    if @page.is_homepage
      flash.delete("alert alert-success")
      flash["alert alert-warning"] = "Homepage cannot be deleted"
    else
      @page.update_attributes(status: "Deleted")
    end
    redirect_to admin_pages_path
  end

  private
    def page_params
      params.require(:page).permit!
    end

    def get_page
      @page = current_user.pages.friendly.find params[:id]
    end
end
