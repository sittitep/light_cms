class Admin::DashboardController < Admin::BaseController
  add_breadcrumb "Dashboard", :admin_dashboard_index_path
  before_filter :set_active_sidebar

  def set_active_sidebar
    @active_sidebar = :dashboard  
  end

  def index
    
  end
end
