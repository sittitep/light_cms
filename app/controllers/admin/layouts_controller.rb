class Admin::LayoutsController < Admin::BaseController
  add_breadcrumb "Layout", :admin_layouts_path

  before_filter :set_active_sidebar
  before_filter :get_layout, only: [:edit,:update,:destroy]
  before_filter :flash_success, only: [:create,:update,:destroy]

  before_filter :only => [:update, :destroy] do |controller|
    controller.instance_eval do
      authorize(@layout)
    end
  end

  def set_active_sidebar
    @active_sidebar = :layout  
  end

  def index
    @layouts = current_user.layouts
  end

  def new
    @layout = Layout.new
    add_breadcrumb "New", :new_admin_layout_path
  end

  def create
    @layout = current_user.layouts.new layout_params
    if @layout.save
      redirect_to edit_admin_layout_path(@layout)
    else
      flash_errors(@layout)
      render 'new'
    end
  end

  def edit
    @stylesheets = @layout.stylesheets
    add_breadcrumb @layout.name, :edit_admin_layout_path
  end

  def update
    
    update_asset("stylesheet", @layout, layout_params["stylesheet_ids"]) if layout_params["stylesheet_update"]
    update_asset("script", @layout, layout_params["script_ids"]) if layout_params["script_update"]

    if @layout.update_attributes layout_params
      redirect_to edit_admin_layout_path(@layout)
    else
      flash_errors(@layout)
      render 'new'
    end
  end

  def destroy
    if @layout.pages.alive.blank?
      @layout.delete
    else
      flash.delete("alert alert-success")
      flash["alert alert-warning"] = "There are some pages using this layout!"
    end
    redirect_to admin_layouts_path
  end

  private
    def layout_params
      params.require(:layout).permit!
    end

    def get_layout
      @layout = Layout.find params[:id]
    end
end
