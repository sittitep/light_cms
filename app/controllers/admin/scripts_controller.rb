class Admin::ScriptsController < Admin::BaseController
  before_filter :get_script, only: [:edit,:update,:destroy]
  before_filter :set_active_sidebar
  before_filter :flash_success, only: [:create,:update,:destroy]

  before_filter :only => [:update, :destroy] do |controller|
    controller.instance_eval do
      authorize(@script)
    end
  end
  
  def set_active_sidebar
    @active_sidebar = :javascript  
  end

  def index
    @scripts = current_user.scripts
  end

  def new
    @script = current_user.scripts.new
    parent_breadcrumb
    add_breadcrumb "New", :new_admin_script_path
  end

  def create
    @script = current_user.scripts.new script_params

    if @script.save
      redirect_to edit_admin_script_path(@script)
    else
      flash_errors(@script)
      render "new"
    end
  end

  def edit
    parent_breadcrumb
    add_breadcrumb "#{@script.name}", edit_admin_script_path(@script)
  end

  def update
    if @script.update_attributes script_params
      redirect_to edit_admin_script_path(@script)
    else
      flash_errors(@script)
      render "new"
    end
  end

  def destroy
    @script.delete
    redirect_to admin_scripts_path
  end

  private

    def script_params
      params.require(:script).permit!
    end

    def get_script
      @script = Script.find params[:id]
    end

    def parent_breadcrumb
      add_breadcrumb "Script", admin_scripts_path
    end

end
