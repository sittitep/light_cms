class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_filter :check_domain!

  def check_domain!
    if request.domain != "bob-cms.com" && (request.subdomain == "" || "www")
        if @current_client = User.find_by_domain(request.domain)
          redirect_to client_page_path(id: @current_client.homepage.slug), subdomain: "www"
        end
    else
      if controller_name == "home" && action_name == "index"
        subdomain = request.subdomain
        @current_client = User.find_by_username(subdomain)
        if @current_client.present? && subdomain != "www"
          if @current_client.pages.present?
            redirect_to client_page_path(id: @current_client.homepage.slug), subdomain: subdomain
          else
            redirect_to "http://www.bob-cms.com"
          end
        end
      end
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :username
  end

end
