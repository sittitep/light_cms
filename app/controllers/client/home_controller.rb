class Client::HomeController < Client::BaseController
  before_filter :get_tags
  
  def index
    if @current_client.is_premium?
      @posts = @current_client.pages.published
      render "/client/#{@current_client.username}/home/index",layout: "/client/#{@current_client.username}"      
    end
  end
end
