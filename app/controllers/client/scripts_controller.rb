class Client::ScriptsController < Client::BaseController
  skip_filter :get_current_client
  skip_filter :get_current_page
  skip_filter :get_layout

  def show
    @script = Script.find params[:id]
    respond_to do |format|
      format.js {}
    end
  end
end
