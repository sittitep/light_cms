class Client::PagesController < Client::BaseController
  
  def show
    if @current_client.is_premium?
      @post = Page.find params[:id]
      render "/client/#{@current_client.username}/posts/show",layout: "/client/#{@current_client.username}"
    else
       @current_page = @current_client.pages.friendly.find(params[:id])
    end
    get_layout
    get_tags
  end
end
