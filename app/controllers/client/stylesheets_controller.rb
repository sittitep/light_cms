class Client::StylesheetsController < Client::BaseController
  skip_filter :get_current_client
  skip_filter :get_current_page
  skip_filter :get_layout

  def show
    @stylesheet = Stylesheet.find params[:id]
    respond_to do |format|
      format.css {}
    end
  end
end
