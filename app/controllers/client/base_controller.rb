class Client::BaseController < ApplicationController
  skip_filter :check_domain!
  before_filter :get_current_client
  layout 'client/custom'
  include Client::BaseHelper

  def get_current_client
    if request.subdomain.present? && request.subdomain != "www"
      @current_client = User.find_by_username(request.subdomain)
    else
      @current_client = User.find_by_domain(request.domain)
    end
    redirect_to root_path unless @current_client.present?
  end

  def get_current_page
    params[:id].present? ? @current_page = @current_client.pages.find_by_id(params[:id]) : @current_page = @current_client.homepage
  end

  private
    def get_layout
      @current_layout = @current_page.layout
    end

    def get_tags
      set_meta_tags title: @current_page.seo_title,
                    description: @current_page.seo_description,
                    keywords: @current_page.seo_keywords,
                    noindex: @current_page.seo_noindex,
                    nofollow: @current_page.seo_nofollow,
                    canonical: @current_page.seo_canonical,
                    prev: @current_page.seo_prev,
                    :next => @current_page.seo_next
    end

end
