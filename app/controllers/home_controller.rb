class HomeController < ApplicationController
  def index
    if @current_client.present?
      if @current_client.is_premium?
        @posts = @current_client.pages.published
        render "/client/#{@current_client.username}/home/index",layout: "/client/#{@current_client.username}"
      end
    else

    end
  end
end
