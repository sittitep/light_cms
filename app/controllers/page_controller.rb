class PageController < ApplicationController
  def show
    @post = Page.find params[:id]
    if @current_client.is_premium?
      render "/client/#{@current_client.username}/posts/show",layout: "/client/#{@current_client.username}"
    else

    end
  end
end
