class ResourceStylesheet < ActiveRecord::Base
  belongs_to :styleable, polymorphic: true
  belongs_to :stylesheet
end
