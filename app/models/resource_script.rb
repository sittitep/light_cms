class ResourceScript < ActiveRecord::Base
  belongs_to :scriptable, polymorphic: true
  belongs_to :script
end
