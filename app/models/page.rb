class Page < ActiveRecord::Base
  validates :title, :presence => true, :uniqueness => {scope: :user}
  validates :slug, :presence => true, :uniqueness => {scope: :user}


  include Rails.application.routes.url_helpers
  scope :alive, lambda { where('status != "Deleted"') }
  scope :published, lambda { where('status = "Published"') }

  has_many :resource_stylesheets, as: :styleable
  has_many :resource_scripts, as: :scriptable
  belongs_to :user
  belongs_to :layout

  attr_accessor :stylesheet_ids, :script_ids, :stylesheet_update, :script_update

  extend FriendlyId
  friendly_id :title, use: :slugged

  def custom_layout
    layout.content
  end

  def stylesheets
    resource_stylesheets.map{ |x| x.stylesheet}
  end

  def scripts
    resource_scripts.map{ |x| x.script}
  end
end
