class Layout < ActiveRecord::Base
  CATEGORY = ["Main","Partial"]

  validates :name, :presence => true, :uniqueness => {scope: :user}

  scope :scope_partial, lambda { where('category = "Partial"') }
  scope :scope_main, lambda { where('category = "Main"') }

  has_many :pages
  has_many :resource_stylesheets, as: :styleable
  has_many :resource_scripts, as: :scriptable
  belongs_to :user

  attr_accessor :stylesheet_ids, :script_ids, :stylesheet_update, :script_update

  def get_patial_layouts
    partial_name = content.scan(/\w+/).map{|w| w.gsub("partial_","") if w.match("partial")}.compact!
    user.layouts.where(name: partial_name)
  end

  def stylesheets
    resource_stylesheets.map{ |x| x.stylesheet}
  end

  def scripts
    resource_scripts.map{ |x| x.script}
  end
end
