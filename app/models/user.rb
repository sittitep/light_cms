class User < ActiveRecord::Base
  validates :username, :presence => true, :uniqueness => true
  validates :domain, :uniqueness => true, :allow_nil => true, :allow_blank => true
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :pages
  has_many :layouts
  has_many :stylesheets
  has_many :scripts

  def is_premium?
    package_id == 999
  end

  def homepage
    pages.published.find_by_is_homepage(true)
  end

  def usable_liquid_variable
    hash = {}
    partial_layouts.each do |partial_layout|
      hash.merge!(partial_layout)
    end
    return hash
  end
  
  def partial_layouts
    array = []
    array = layouts.scope_partial.scope_partial.map{ |l| {"partial_"+l.name => l.content}}
  end

end