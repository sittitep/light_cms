module Admin::BaseHelper
  def admin_sidebar
    menu = ""
    Admin::BaseController::MENU.each do |x|
      menu_class = "list-group-item"
      menu_class += " active" if x[:name] == @active_sidebar
      menu += link_to(t('admin.'+x[:name].to_s), x[:path], class: menu_class)
    end
    return menu.html_safe
  end

  def page_link(page)
    url_for controller: 'client/pages',
            action: 'show',
            id: page.slug,
            subdomain: page.user.username,
            only_path: false

  end
end
