module Client::BaseHelper
  def css_link(current_page,current_layout)

    partial_layouts = current_layout.get_patial_layouts
    partial_stylesheets = []
    partial_layouts.each do |partial_layout|
      partial_stylesheets += partial_layout.stylesheets
    end

    stylesheets = current_layout.stylesheets + partial_stylesheets + current_page.stylesheets
    css_link = ""
    stylesheets.each do |stylesheet|
      if stylesheet.cdn_link.present?
        url = stylesheet.cdn_link
        css_link += "<link href='#{url}' rel='stylesheet' type='text/css' />"
      else
        url = url_for :controller => 'client/stylesheets',
                      :action => 'show', :id => stylesheet.id, 
                      :subdomain => false, 
                      :only_path => false,
                      :format => :css
        css_link += "<link href='#{url}' rel='stylesheet' type='text/css' />"
      end
    end
    return css_link.html_safe
  end

  def script_link(current_page,current_layout)

    partial_layouts = current_layout.get_patial_layouts
    partial_scripts = []
    partial_layouts.each do |partial_layout|
      partial_scripts += partial_layout.scripts
    end

    scripts = current_layout.scripts + partial_scripts +current_page.scripts
    script_link = ""
    scripts.each do |script|
      if script.cdn_link.present?
        url = script.cdn_link
        script_link += "<script src='#{url}'></script>"
      else
        url = url_for :controller => 'client/scripts',
                      :action => 'show', :id => script.id, 
                      :subdomain => false, 
                      :only_path => false,
                      :format => :js
        script_link += "<script src='#{url}'></script>"
      end
    end
    return script_link.html_safe
  end
end