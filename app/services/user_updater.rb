class UserUpdater
  def initialize(user, params)
    @user = user

    update_domain!(@user.domain, params["domain"])

    @user.assign_attributes(params)
  end

  def update_domain!(current_domain, new_domain)
    if User.find_by_domain(new_domain).blank?
      if new_domain.present?
        if new_domain != current_domain
          # Digitalocean::Domain.destroy(@user.domain_id)
          HTTParty.get("https://api.digitalocean.com/domains/#{@user.domain_id}/destroy?client_id=#{Digitalocean.client_id}&api_key=#{Digitalocean.api_key}") if current_domain.present?
          response = Digitalocean::Domain.create({name: new_domain, ip_address: "128.199.213.62"})
          @user.domain_id = response.domain.id
          HTTParty.get("https://api.digitalocean.com/domains/#{@user.domain_id}/records/new?client_id=#{Digitalocean.client_id}&api_key=#{Digitalocean.api_key}&record_type=A&data=128.199.213.62&name=*")
          update_nginx_file!(:destroy,current_domain)
          update_nginx_file!(:create,new_domain)
        end
      else
        Digitalocean::Domain.destroy(@user.domain_id)
        update_nginx_file!(:destroy,current_domain)
      end
    end
  end

  def update_nginx_file!(action,value)
    Setting.custom_domain_conf = "" unless Setting.custom_domain_conf.present?
    conf = Setting.custom_domain_conf
    case action
    when :create
      conf += "server {listen 80; server_name *.#{value}; root /var/www/light_cms/current/public; passenger_enabled on;} "
    when :destroy
      conf = conf.gsub("server {listen 80; server_name *.#{value}; root /var/www/light_cms/current/public; passenger_enabled on;} ","")
    end
    Setting.custom_domain_conf = conf
    Rails.env.development? ? path = "/Users/sittitep/new.txt" : path = "/var/www/light_cms/shared/custom_domain.conf"
    conf_file = File.open(path,"w")
    conf_file.puts conf
    conf_file.close
  end

  def user
    @user
  end
end